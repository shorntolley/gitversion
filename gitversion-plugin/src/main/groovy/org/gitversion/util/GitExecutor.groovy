package org.gitversion.util

import org.apache.tools.ant.taskdefs.condition.Os

import static org.apache.tools.ant.taskdefs.condition.Os.FAMILY_WINDOWS

/**
 * Is there really no better way to do the whole "find executable" logic,
 * there must be a library or framework somewhere that would do a better job
 * than this.
 */
class GitExecutor {


  String providedGitBinary
  String versionPrefix
  GvLogger log

  private String gitPathLocation = "git"
  private String[] gitBinaryWindowsLocations = [
    "C:\\PROGRA~1\\Git\\bin\\git",  // C:\Program Files
    "C:\\PROGRA~2\\Git\\bin\\git",  // Program Files (x86)
    "C:\\apps\\git\\bin\\git"       // my personal weirdness
  ]

  /**
   * No kind of checking will be done - if gitBinary providedGitBinary is set,
   * then is is used - if it fails, that's life.
   */
  boolean forceProvidedGitBinary = false

  /**
   * When searching for a gitBinary in that absense of a working
   * providedGitBinary, try just looking for the git on the path.
   */
  boolean tryGitPath = true

  /**
   * Only defaults to only using annotated tags (created with "git tag -a")
   * and I like that so that we can see the metadata of tag creation (who,
   * when, etc.)
   */
  String getDescribe(){
    assert versionPrefix
    String gitTagArgs= "--match ${versionPrefix}*"
    log.debug "git tag match arg: `${gitTagArgs}`"

    // "--always" just in case the version tag somehow doesn't exist
    // "--dirty" to show if there are any uncommitted changes
    String gitFormatArgs = "--always --dirty"

    String command = "${findGitBinary()} describe  $gitFormatArgs $gitTagArgs"
    log.debug "command: $command"
    def proc = command.execute()
    proc.waitFor()
    String commandResult = proc.in.text.trim()
    String versionResult = commandResult - versionPrefix
    log.debug "extracted version `$versionResult` from: `$commandResult`"
    return versionResult
  }

  String getCommitId(boolean useShort=true){
    String shortSpec = useShort ? "--short" : ""
    String command = "${findGitBinary()} rev-parse $shortSpec HEAD"
    log.debug "command: $command"

    def proc = command.execute()
    proc.waitFor()
    String commandResult = proc.in.text.trim()
    String versionResult = commandResult - versionPrefix
    return versionResult
  }

  boolean isGitVersionWorks(String binary){
    String command = "${binary} version"
    log.debug "checking git with command: $command"
    String commandResult
    try {
      def proc = command.execute()
      proc.waitFor()
      commandResult = proc.in.text.trim()
    }
    catch( IOException ioe ){
      // we're assuming this is somethign like:
      // "CreateProcess error=2, The system cannot find the file specified"
      return false
    }
    // result on windows: "git version 2.5.0.windows.1
    // result on linux: "???"
    return commandResult.startsWith("git version")
  }

  /**
   * Does its best to find a working version of git somewhere.
   * Will try the path.
   * On windows will search some known install locations.
   * There's other cases too; use the source, Luke.
   */
  String findGitBinary(){
    if( providedGitBinary && forceProvidedGitBinary ){
      log.debug "forcing the provided git binary to be used without checking"
      return providedGitBinary
    }

    if( providedGitBinary ) {
      if( isGitVersionWorks(providedGitBinary) ){
        return providedGitBinary
      } else {
        log.debug "providedGitBinary didn't work, trying" +
          " other locations : $providedGitBinary"
      }
    }

    if( tryGitPath && isGitVersionWorks(gitPathLocation) ){
      if( isGitVersionWorks(gitPathLocation) ){
        return gitPathLocation
      }
      else {
        log.debug ""
      }
    }

    if( Os.isFamily(FAMILY_WINDOWS) ){
      String workingBinary =
        gitBinaryWindowsLocations.find{ isGitVersionWorks(it) }
      if( workingBinary ){
        log.debug "found a working binary at: ${workingBinary}"
        return workingBinary
      }
    }

    throw new UnsupportedOperationException("no git binary could be found")
  }

}
