package org.gitversion.util

import org.gitversion.plugin.GitversionPlugin
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.gradle.api.logging.Logging

class GvLogger {
  Logger buildLogger = Logging.getLogger(GitversionPlugin.class);

  boolean debug = true

  void output(String s){
    println s
  }

  void debug(String s){
    if( debug ){
      println s
      buildLogger.info(s)
    }
    else {
      buildLogger.debug(s)
    }
  }

}
