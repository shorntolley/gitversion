package org.gitversion.plugin

import org.gitversion.util.GitExecutor
import org.gitversion.util.GvLogger
import org.gradle.api.Plugin;
import org.gradle.api.Project;

public class GitversionPlugin implements Plugin<Project> {

  GitversionPluginExtensionImplementation gitversion

  @Override
  void apply(Project project) {

    gitversion = project.extensions.create(
      "gitversion", GitversionPluginExtensionImplementation)
//    gitversion = project.gitversion
    gitversion.init(this, project)

    project.task('printGvVersion') {
      description = "print the version of gitVersion plugin used"
      group = "gitversion"
      doLast {
        // depends on impl version being set correctly in the Manifest file,
        // see jar task in plugin gradle file
        println "Gitversion plugin version: " +
          getClass().getPackage().getImplementationVersion()
      }
    }

    project.task('printVersion') {
      description = "print the version of the project"
      group = "gitversion"
      doLast {
        String gvVersion = gitversion.version
        String projectVersion = project.version

        if( projectVersion == gvVersion ){
          println "project version: " + projectVersion
        }
        else {
          println "gitversion version: " + gvVersion
          println "project version: " + projectVersion
          println "(consider adding 'project.version = gitversion.version'" +
            " to your project)"
        }
      }
    }

  }

}
