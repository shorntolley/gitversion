package org.gitversion.plugin

import org.gitversion.util.GitExecutor
import org.gitversion.util.GvLogger
import org.gradle.api.Project

abstract class GitversionPluginExtension {
  /** Set this to promote plugin debug messages to info level (you should see
   * them on the console.)
   * Otherwise the messages are logged at debug level, you can see them by
   * running gradle with the "-d" switch (but you'll see a lot of other debug
   * messages from the rest of gradle too).
   */
  boolean debug = false

  /** this is expected to be glob(7) format */
  String versionPrefix = "version."

  /**
   * Defaults to looking for git on the path, set system property
   * 'gitversion.gitBinary' in order to provide the location of the git
   * executable binary (path to the file itself, not just the directory).
   * <p/>
   * Recommend setting the property in your ~/.gradle/gradle.properties file,
   * so it will be set for all projects that use this plugin. The line in the
   * file should be something like
   * "systemProp.gitversion.gitBinary=C:/Progra~1/Git/bin/git".
   */
  String gitBinary = System.getProperty("gitversion.gitBinary", "git")

  /**
   * This controls the behaviour of {@link #getCommitId()} only - has no
   * effect on the "version".
   */
  boolean shortCommitId = false

  /**
   * if set to false, force git to re-execute every command.
   */
  boolean cacheGitCommitCommands = true

  /**
   * if true, {@link #getCommitId()} will add {@link #dirtyCommitIdIndicator}
   * to the end of the commit id.
   */
  boolean indicateDirtyCommitId = true

  /** @see #indicateDirtyCommitId */
  String dirtyCommitIdIndicator = "+"

  /**
   * Only uses annotated tags (created with "git tag -a") so that we can see
   * the metadata of tag creation (who, when, etc.)
   */
  abstract String getVersion();

  /**
   * returns the git commit id of the workind directory.
   */
  abstract String getCommitId();

  /**
   * Returns true if version ends with "-dirty", meaning there are uncommitted
   * changes in the working directory.
   */
  abstract boolean isDirty();

  /**
   * !isDirty
   * @see #isDirty()
   */
  abstract boolean isClean();


}
